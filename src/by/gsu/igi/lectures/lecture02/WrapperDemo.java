package by.gsu.igi.lectures.lecture02;

/**
 * Created by Evgeniy Myslovets.
 */
public class WrapperDemo {

    public static void main(String[] args) {
        int x = 6;

        Integer bigInt = x;

        System.out.println(x);
        System.out.println(bigInt);

        bigInt = 7;

        System.out.println(x);
        System.out.println(bigInt);

        int y = bigInt;
        System.out.println(y);
    }
}
